package com.example.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.DemandePlongee;

public interface DemandePlongeeRepository extends JpaRepository<DemandePlongee,Long> {
	
	@Query("select d from DemandePlongee d where d.nom like %:x%")
	public Page<DemandePlongee> chercher(@Param("x") String mc, Pageable pageable);
	
	DemandePlongee findByNum(Long num);
}
