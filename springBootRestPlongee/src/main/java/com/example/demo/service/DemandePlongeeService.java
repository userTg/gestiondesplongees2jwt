package com.example.demo.service;

import java.util.Collection;

import com.example.demo.model.DemandePlongee;

public interface DemandePlongeeService {
	
	Collection<DemandePlongee> getAllDemandePlongee();
	DemandePlongee saveOrUpdateDemandePlongee(DemandePlongee demandePlongeeToUpdate);
	void deleteDemandePlongee(Long num);
	DemandePlongee getDemandePlongeeById(Long num);

}
