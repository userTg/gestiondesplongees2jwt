package com.example.demo.service;

import java.util.Collection;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.DemandePlongee;
import com.example.demo.repository.DemandePlongeeRepository;

@Service(value = "demandePlongeeService")

public class DemandePlongeeServiceImpl implements DemandePlongeeService {
	
	@Autowired
	private DemandePlongeeRepository demandePlongeeRepository;

	@Override
	public Collection<DemandePlongee> getAllDemandePlongee() {
		return IteratorUtils.toList(demandePlongeeRepository.findAll().iterator());
	}

	@Override
	public DemandePlongee saveOrUpdateDemandePlongee(DemandePlongee demandePlongeeToUpdate) {
		return demandePlongeeRepository.save(demandePlongeeToUpdate);
	}

	@Override
	public void deleteDemandePlongee(Long num) {
		demandePlongeeRepository.deleteById(num);

	}

	@Override
	public DemandePlongee getDemandePlongeeById(Long num) {
		return demandePlongeeRepository.findByNum(num);
	}

}
