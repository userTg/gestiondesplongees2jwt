package com.example.demo.service;

import java.util.Collection;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Plongeur;
import com.example.demo.repository.PlongeurRepository;


@Service(value = "plongeurService")
public class PlongeurServiceImpl implements PlongeurService {
	
	@Autowired
	private PlongeurRepository plongeurRepository;

	@Override
	public Collection<Plongeur> getAllPlongeur() {
		return IteratorUtils.toList(plongeurRepository.findAll().iterator());
	}

	@Override
	public Plongeur getPlongeurById(Long num) {
		return plongeurRepository.findByNum(num);
	}

	@Override
	@Transactional(readOnly=false)
	public Plongeur saveOrUpdatePlongeur(Plongeur plongeur) {
		return plongeurRepository.save(plongeur);
	}

	@Override
	public void deletePlongeur(Long id) {
		plongeurRepository.deleteById(id);


	}


}
