package com.example.demo.controller;

import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.model.DemandePlongee;
import com.example.demo.service.DemandePlongeeService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v3")
public class DemandePlongeeRestController {

	@Autowired
	private DemandePlongeeService demandePlongeeService;

	@GetMapping(value = "/demandes")
	public Collection<DemandePlongee> getDemandePlongees() {
		return demandePlongeeService.getAllDemandePlongee();
	}

	@GetMapping(value = "/demandes/{id}")
	public ResponseEntity<DemandePlongee> getDemandePlongeeById(@PathVariable Long id) {
		DemandePlongee demandePlongee = demandePlongeeService.getDemandePlongeeById(id);
		if (demandePlongee == null) {

			throw new ResourceNotFoundException("demandePlongee not found : " + id);
		}
		return ResponseEntity.ok().body(demandePlongee);
	}

	@PostMapping(value = "/demandes", produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public DemandePlongee save(@Valid @RequestBody DemandePlongee demandePlongee) {
		return demandePlongeeService.saveOrUpdateDemandePlongee(demandePlongee);
	}

	@PutMapping("/demandes/{id}")
	@Transactional
	public ResponseEntity<DemandePlongee> updateDemandePlongee(@PathVariable(value = "id") Long id,
			@RequestBody DemandePlongee demandePlongee) {
		DemandePlongee demandePlongeeToUpdate = demandePlongeeService.getDemandePlongeeById(id);
		if (demandePlongeeToUpdate == null) {
			throw new ResourceNotFoundException("DemandePlongee not found : " + id);
		}
		demandePlongeeToUpdate.setNom(demandePlongee.getNom());
		demandePlongeeToUpdate.setPrenom(demandePlongee.getPrenom());
		demandePlongeeToUpdate.setNiveau(demandePlongee.getNiveau());
		demandePlongeeToUpdate.setDatePlongee(demandePlongee.getDatePlongee());
		demandePlongeeToUpdate.setTypePlongee(demandePlongee.getTypePlongee());
		demandePlongeeToUpdate.setCertifMedical(demandePlongee.getCertifMedical());
		demandePlongeeToUpdate.setLocationMateriel(demandePlongee.getLocationMateriel());
		DemandePlongee demandePlongeeUpdated = demandePlongeeService.saveOrUpdateDemandePlongee(demandePlongeeToUpdate);
		return new ResponseEntity<DemandePlongee>(demandePlongeeUpdated, HttpStatus.OK);
	}

	@DeleteMapping("/demandes/{id}")
	@Transactional
	public void deleteDemande(@PathVariable(value = "id") Long id) {
		DemandePlongee demandeToDelete = demandePlongeeService.getDemandePlongeeById(id);
		if (demandeToDelete == null) {
			throw new ResourceNotFoundException("DemandePlongee not found : " + id);
		}
		demandePlongeeService.deleteDemandePlongee(id);
	}

}
