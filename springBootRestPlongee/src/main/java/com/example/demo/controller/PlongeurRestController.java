package com.example.demo.controller;

import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.model.Plongeur;
import com.example.demo.service.PlongeurService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v3")
public class PlongeurRestController {
	
	@Autowired
	private PlongeurService plongeurService;
	
	@GetMapping(value = "/plongeurs")
	public Collection<Plongeur> getPlongeurs() {
	return plongeurService.getAllPlongeur();
	}
		
	
	@GetMapping(value = "/plongeurs/{id}")
	public ResponseEntity<Plongeur> getPlongeurById(@PathVariable Long id) {

		Plongeur plongeur = plongeurService.getPlongeurById(id);
		if (plongeur == null) {

			throw new ResourceNotFoundException("Plongeur not found : " + id);
		}
		
		
         return ResponseEntity.ok().body(plongeur);

	}
	
	@PostMapping(value = "/plongeurs", produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public Plongeur save(@Valid @RequestBody Plongeur plongeur) {

       return plongeurService.saveOrUpdatePlongeur(plongeur);

	}
	
	 @PutMapping("/plongeurs/{id}")
		@Transactional
		public ResponseEntity<Plongeur> updatePlongeur(@PathVariable(value = "id") Long id,
				@RequestBody Plongeur plongeur) {
		 Plongeur plongeurToUpdate = plongeurService.getPlongeurById(id);
			if (plongeurToUpdate == null) {
				throw new ResourceNotFoundException("Plongeur not found : " + id);
			}
			plongeurToUpdate.setNom(plongeur.getNom());
			plongeurToUpdate.setPrenom(plongeur.getPrenom());
			plongeurToUpdate.setDatePlongee(plongeur.getDatePlongee());
			plongeurToUpdate.setNiveau(plongeur.getNiveau());
			plongeurToUpdate.setProfondeur(plongeur.getProfondeur());
			plongeurToUpdate.setDureePlongee(plongeur.getDureePlongee());
			plongeurToUpdate.setDescription(plongeur.getDescription());
			Plongeur plongeurUpdated = plongeurService.saveOrUpdatePlongeur(plongeurToUpdate);
			return new ResponseEntity<Plongeur>(plongeurUpdated, HttpStatus.OK);
		}
	 
	 @DeleteMapping("/plongeurs/{id}")
		@Transactional
		public void deletePlongeur(@PathVariable(value = "id") Long id) {
			Plongeur plongeurToDelete = plongeurService.getPlongeurById(id);
			
			if (plongeurToDelete == null) {
				throw new ResourceNotFoundException("Plongeur not found : " + id);
			}

			plongeurService.deletePlongeur(id);
		}

}
