package com.example.demo.dto;

import java.io.Serializable;
import java.time.LocalDate;

public class DemandePlongeeDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String prenom;
	private String nom;
	private String niveau;
	private LocalDate datePlongee;
	private String typePlongee;
	private Boolean certifMedical;
	private Boolean locationMateriel;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getNiveau() {
		return niveau;
	}
	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	public LocalDate getDatePlongee() {
		return datePlongee;
	}
	public void setDatePlongee(LocalDate datePlongee) {
		this.datePlongee = datePlongee;
	}
	public String getTypePlongee() {
		return typePlongee;
	}
	public void setTypePlongee(String typePlongee) {
		this.typePlongee = typePlongee;
	}
	public Boolean getCertifMedical() {
		return certifMedical;
	}
	public void setCertifMedical(Boolean certifMedical) {
		this.certifMedical = certifMedical;
	}
	public Boolean getLocationMateriel() {
		return locationMateriel;
	}
	public void setLocationMateriel(Boolean locationMateriel) {
		this.locationMateriel = locationMateriel;
	}
	public DemandePlongeeDto(Long id, String prenom, String nom, String niveau, LocalDate datePlongee, String typePlongee,
			Boolean certifMedical, Boolean locationMateriel) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.niveau = niveau;
		this.datePlongee = datePlongee;
		this.typePlongee = typePlongee;
		this.certifMedical = certifMedical;
		this.locationMateriel = locationMateriel;
	}
	public DemandePlongeeDto(String prenom, String nom, String niveau, LocalDate datePlongee, String typePlongee,
			Boolean certifMedical, Boolean locationMateriel) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.niveau = niveau;
		this.datePlongee = datePlongee;
		this.typePlongee = typePlongee;
		this.certifMedical = certifMedical;
		this.locationMateriel = locationMateriel;
	}
	@Override
	public String toString() {
		return "DemandePlongeeDto [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", niveau=" + niveau
				+ ", datePlongee=" + datePlongee + ", typePlongee=" + typePlongee + ", certifMedical=" + certifMedical
				+ ", locationMateriel=" + locationMateriel + "]";
	}
	
	

}
