package com.example.demo.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.example.demo.dto.DemandePlongeeDto;

@Entity
@Table(name = "demandePlongee")
public class DemandePlongee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long num;
	
	@Column(name = "nom", nullable = false)
	private String nom;
	
	
	@Column(name = "prenom", nullable = false)
	private String prenom;
	
	@Column(name = "niveau")
	private String niveau;

	@Column(name = "datePlongee")
	private LocalDate datePlongee;
	
	@Column(name = "typePlongee")
	private String typePlongee;
	
	@Column(name = "certifMedical")
	private Boolean certifMedical;
	
	@Column(name = "locationMateriel")
	private Boolean locationMateriel;

	public Long getNum() {
		return num;
	}

	public void setNum(Long num) {
		this.num = num;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	public LocalDate getDatePlongee() {
		return datePlongee;
	}

	public void setDatePlongee(LocalDate datePlongee) {
		this.datePlongee = datePlongee;
	}

	public String getTypePlongee() {
		return typePlongee;
	}

	public void setTypePlongee(String typePlongee) {
		this.typePlongee = typePlongee;
	}

	public Boolean getCertifMedical() {
		return certifMedical;
	}

	public void setCertifMedical(Boolean certifMedical) {
		this.certifMedical = certifMedical;
	}

	public Boolean getLocationMateriel() {
		return locationMateriel;
	}

	public void setLocationMateriel(Boolean locationMateriel) {
		this.locationMateriel = locationMateriel;
	}
	
	

	public DemandePlongee() {
		super();
	}

	public DemandePlongee(String nom, String prenom, String niveau, LocalDate datePlongee, String typePlongee,
			Boolean certifMedical, Boolean locationMateriel) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.niveau = niveau;
		this.datePlongee = datePlongee;
		this.typePlongee = typePlongee;
		this.certifMedical = certifMedical;
		this.locationMateriel = locationMateriel;
	}
	
	public DemandePlongee(DemandePlongeeDto demandePlongeeDto) {
		this.setNum(demandePlongeeDto.getId());
		this.setPrenom(demandePlongeeDto.getPrenom());
		this.setNom(demandePlongeeDto.getNom());
		this.setNiveau(demandePlongeeDto.getNiveau());
		this.setDatePlongee(demandePlongeeDto.getDatePlongee());
		this.setTypePlongee(demandePlongeeDto.getTypePlongee());
		this.setCertifMedical(demandePlongeeDto.getCertifMedical());
		this.setLocationMateriel(demandePlongeeDto.getLocationMateriel());
	}

	@Override
	public String toString() {
		return "DemandePlongee [num=" + num + ", nom=" + nom + ", prenom=" + prenom + ", niveau=" + niveau
				+ ", datePlongee=" + datePlongee + ", typePlongee=" + typePlongee + ", certifMedical=" + certifMedical
				+ ", locationMateriel=" + locationMateriel + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((certifMedical == null) ? 0 : certifMedical.hashCode());
		result = prime * result + ((datePlongee == null) ? 0 : datePlongee.hashCode());
		result = prime * result + ((locationMateriel == null) ? 0 : locationMateriel.hashCode());
		result = prime * result + ((niveau == null) ? 0 : niveau.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((num == null) ? 0 : num.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + ((typePlongee == null) ? 0 : typePlongee.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DemandePlongee other = (DemandePlongee) obj;
		if (certifMedical == null) {
			if (other.certifMedical != null)
				return false;
		} else if (!certifMedical.equals(other.certifMedical))
			return false;
		if (datePlongee == null) {
			if (other.datePlongee != null)
				return false;
		} else if (!datePlongee.equals(other.datePlongee))
			return false;
		if (locationMateriel == null) {
			if (other.locationMateriel != null)
				return false;
		} else if (!locationMateriel.equals(other.locationMateriel))
			return false;
		if (niveau == null) {
			if (other.niveau != null)
				return false;
		} else if (!niveau.equals(other.niveau))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (num == null) {
			if (other.num != null)
				return false;
		} else if (!num.equals(other.num))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (typePlongee == null) {
			if (other.typePlongee != null)
				return false;
		} else if (!typePlongee.equals(other.typePlongee))
			return false;
		return true;
	}
	
	
	
	
}
