package com.example.demo.model;
import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.example.demo.dto.PlongeurDto;


@Entity
@Table(name = "plongeur")
public class Plongeur implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long num;
	
	@Column(name = "nom", nullable = false)
	private String nom;
	
	
	@Column(name = "prenom", nullable = false)
	private String prenom;
	
	@Column(name = "niveau")
	private String niveau;
	
	@Column(name = "profondeur")
	private int profondeur;
	
	@Column(name = "datePlongee")
	private LocalDate datePlongee;
	
	@Column(name = "dureePlongee")
	private String dureePlongee;
	
	@Column(name = "description")
	private String description;


	public Plongeur(PlongeurDto plongeurDto) {
		this.setNum(plongeurDto.getId());
		this.setPrenom(plongeurDto.getPrenom());
		this.setNom(plongeurDto.getNom());
		this.setNiveau(plongeurDto.getNiveau());
		this.setDatePlongee(plongeurDto.getDatePlongee());
		this.setProfondeur(plongeurDto.getProfondeur());
		this.setDureePlongee(plongeurDto.getDureePlongee());
		this.setDescription(plongeurDto.getDescription());
	}
	
	

	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getDureePlongee() {
		return dureePlongee;
	}



	public void setDureePlongee(String dureePlongee) {
		this.dureePlongee = dureePlongee;
	}



	public Plongeur() {
		super();
	}



	public Plongeur(String nom, String prenom, String niveau, int profondeur, LocalDate datePlongee, String dureePlongee, String description) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.niveau = niveau;
		this.profondeur = profondeur;
		this.datePlongee = datePlongee;
		this.dureePlongee = dureePlongee;
		this.description = description;
	}

	public Long getNum() {
		return num;
	}

	public void setNum(Long id) {
		this.num = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	public int getProfondeur() {
		return profondeur;
	}

	public void setProfondeur(int profondeur) {
		this.profondeur = profondeur;
	}

	public LocalDate getDatePlongee() {
		return datePlongee;
	}

	public void setDatePlongee(LocalDate datePlongee) {
		this.datePlongee = datePlongee;
	}



	@Override
	public String toString() {
		return "Plongeur [num=" + num + ", nom=" + nom + ", prenom=" + prenom + ", niveau=" + niveau + ", profondeur="
				+ profondeur + ", datePlongee=" + datePlongee + ", dureePlongee=" + dureePlongee + ", description="
				+ description + "]";
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datePlongee == null) ? 0 : datePlongee.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((dureePlongee == null) ? 0 : dureePlongee.hashCode());
		result = prime * result + ((niveau == null) ? 0 : niveau.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((num == null) ? 0 : num.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + profondeur;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plongeur other = (Plongeur) obj;
		if (datePlongee == null) {
			if (other.datePlongee != null)
				return false;
		} else if (!datePlongee.equals(other.datePlongee))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (dureePlongee == null) {
			if (other.dureePlongee != null)
				return false;
		} else if (!dureePlongee.equals(other.dureePlongee))
			return false;
		if (niveau == null) {
			if (other.niveau != null)
				return false;
		} else if (!niveau.equals(other.niveau))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (num == null) {
			if (other.num != null)
				return false;
		} else if (!num.equals(other.num))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (profondeur != other.profondeur)
			return false;
		return true;
	}



	

	
	
	
	

}
